package com.dbshac2hire.thankcoin.ledger.api.domain;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Transaction {
	public enum Type { CREATE, TRANSFER }
	
	private @Id @GeneratedValue Long id;
	private final Type type;
	private final String accountFrom;
	private final String accountTo;
	private final long amount;
	private final Date timestamp;
	private final String description;
	private final String signature;
	
	public Transaction(Type type, String accountFrom, String accountTo, long amount, Date timestamp, String description, String signature) {
		super();
		this.type = type;
		this.accountFrom = accountFrom;
		this.accountTo = accountTo;
		this.amount = amount;
		this.timestamp = timestamp;
		this.description = description;
		this.signature = signature;
	}
	
	@SuppressWarnings("unused")
	private Transaction() {
		id = null;
		type = null;
		accountFrom = null;
		accountTo = null;
		amount = 0;
		timestamp = null;
		description = null;
		signature = null;
	}

	public Long getId() {
		return id;
	}

	public Type getType() {
		return type;
	}

	public String getAccountFrom() {
		return accountFrom;
	}

	public String getAccountTo() {
		return accountTo;
	}

	public long getAmount() {
		return amount;
	}

	public Date getTimestamp() {
		return timestamp;
	}
	
	public String getDescription() {
		return description;
	}

	public String getSignature() {
		return signature;
	}
	
}
