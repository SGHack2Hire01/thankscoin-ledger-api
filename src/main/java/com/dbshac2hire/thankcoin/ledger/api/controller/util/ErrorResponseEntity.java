package com.dbshac2hire.thankcoin.ledger.api.controller.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ErrorResponseEntity extends ResponseEntity<Object> {
	
	public ErrorResponseEntity(String message) {
		this(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public ErrorResponseEntity(String message, HttpStatus httpStatus) {
		super(new ErrorMessage(message), httpStatus);
	}

	private static class ErrorMessage {
		private final String error;

		public ErrorMessage(String error) {
			super();
			this.error = error;
		}

		@SuppressWarnings("unused")
		public String getError() {
			return error;
		}
	}
}
