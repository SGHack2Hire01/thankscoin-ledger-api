package com.dbshac2hire.thankcoin.ledger.api.repository;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.dbshac2hire.thankcoin.ledger.api.domain.Transaction;

public interface TransactionRepository extends Repository<Transaction, Long> {

	/**
	 * Saves the given {@link Transaction}.
	 * 
	 * @param entry
	 *            the entry to save, must not be {@literal null}.
	 * @return the saved {@link Transaction}.
	 */
	Transaction save(Transaction entry);

	/**
	 * Returns the {@link Transaction} with the given id, if it exists,
	 * {@link Optional#empty()} otherwise.
	 * 
	 * @param id
	 *            must not be {@literal null}.
	 * @return
	 */
	Optional<Transaction> findOne(Long id);

	/**
	 * Returns all {@link Transaction}s available.
	 * 
	 * @return
	 */
	Iterable<Transaction> findAll();

	/**
	 * Returns the number of {@link Transaction}s available.
	 * 
	 * @return
	 */
	int count();

	/**
	 * Search by accountFrom or accountBy {@link Transaction}s available.
	 * 
	 * @return
	 */
	Iterable<Transaction> findByAccountFromOrAccountToOrderByTimestampDesc(String accountFrom, String accountTo);

	/**
	 * Search for {@link Transaction} with optional parameters.
	 * 
	 * @return
	 */
	@Query("SELECT t FROM Transaction t where (:accountFrom is null or :accountFrom='' or t.accountFrom = :accountFrom) AND "
			+ "(:accountTo is null or :accountTo='' or t.accountTo = :accountTo) AND "
			+ "(:afterId is null or t.id > :afterId) AND " + "(:amount is null or t.amount = :amount) AND "
			+ "(:txtimeFrom is null or t.timestamp >= :txtimeFrom) AND "
			+ "(:txtimeTo is null or t.timestamp <= :txtimeTo)")
	Iterable<Transaction> search(@Param("accountFrom") String accountFrom, @Param("accountTo") String accountTo,
			@Param("afterId") Long afterId, @Param("amount") Long amount, @Param("txtimeFrom") Date txtimeFrom,
			@Param("txtimeTo") Date txtimeTo);

	/**
	 * Search for {@link Transaction} with optional parameters and balances.
	 * 
	 * @return
	 */
	@Query("SELECT t, a1.balance as acc1balance, a2.balance as acc2balance FROM "
			+ "Transaction t, Account a1, Account a2 WHERE "
			+ "t.accountFrom = a1.number AND t.accountTo = a2.number AND "
			+ "(:accountFrom is null or :accountFrom='' or t.accountFrom = :accountFrom) AND "
			+ "(:accountTo is null or :accountTo='' or t.accountTo = :accountTo) AND "
			+ "(:afterId is null or t.id > :afterId) AND " + "(:amount is null or t.amount = :amount) AND "
			+ "(:txtimeFrom is null or t.timestamp >= :txtimeFrom) AND "
			+ "(:txtimeTo is null or t.timestamp <= :txtimeTo)")
	Iterable<Transaction> extendedSearch(@Param("accountFrom") String accountFrom, @Param("accountTo") String accountTo,
			@Param("afterId") Long afterId, @Param("amount") Long amount, @Param("txtimeFrom") Date txtimeFrom,
			@Param("txtimeTo") Date txtimeTo);
}
