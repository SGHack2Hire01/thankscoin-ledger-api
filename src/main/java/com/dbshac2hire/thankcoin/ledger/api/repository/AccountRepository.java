package com.dbshac2hire.thankcoin.ledger.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.dbshac2hire.thankcoin.ledger.api.domain.Account;

public interface AccountRepository extends Repository<Account, Long> {

	/**
	 * Deletes the {@link Account} with the given id.
	 * 
	 * @param id
	 *            must not be {@literal null}.
	 */
	void delete(Long id);

	/**
	 * Saves the given {@link Account}.
	 * 
	 * @param entry
	 *            the entry to save, must not be {@literal null}.
	 * @return the saved {@link Account}.
	 */
	Account save(Account entry);

	/**
	 * Returns the {@link Account} with the given id, if it exists,
	 * {@link Optional#empty()} otherwise.
	 * 
	 * @param id
	 *            must not be {@literal null}.
	 * @return
	 */
	Optional<Account> findOne(Long id);

	/**
	 * Returns the {@link Account} with the given id, if it exists,
	 * {@link Optional#empty()} otherwise.
	 * 
	 * @param id
	 *            must not be {@literal null}.
	 * @return
	 */
	Optional<Account> findByNumber(String number);

	/**
	 * Returns all {@link Account}s available.
	 * 
	 * @return
	 */
	Iterable<Account> findAll();

	/**
	 * Returns the number of {@link Account}s available.
	 * 
	 * @return
	 */
	int count();

	/**
	 * Returns richest {@link Account}s available.
	 * 
	 * @return
	 */
	List<Account> findAllByOrderByBalanceDesc(Pageable pageable);

	/**
	 * Returns recent {@link Account}s available.
	 * 
	 * @return
	 */
	List<Account> findAllByOrderByCreatedDesc(Pageable pageable);

	/**
	 * Returns recent active {@link Account}s available.
	 * 
	 * @return
	 */
	List<Account> findAllByOrderByLastTransactionDesc(Pageable pageable);

	/**
	 * Returns balance in system
	 * 
	 * @return
	 */
	@Query(value = "select sum(balance) from account where number != 'corporate'", nativeQuery = true)
	public String findSystemBalance();

}
