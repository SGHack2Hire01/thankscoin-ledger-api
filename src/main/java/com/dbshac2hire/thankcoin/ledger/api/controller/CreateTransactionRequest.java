package com.dbshac2hire.thankcoin.ledger.api.controller;

public class CreateTransactionRequest {
	private String accountFrom;
	private String accountTo;
	private Long amount;
	private String description;
	private String signature;

	public String getAccountFrom() {
		return accountFrom;
	}

	public String getAccountTo() {
		return accountTo;
	}

	public Long getAmount() {
		return amount;
	}

	public String getDescription() {
		return description;
	}

	public String getSignature() {
		return signature;
	}

}
