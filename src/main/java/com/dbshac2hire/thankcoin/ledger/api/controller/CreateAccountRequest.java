package com.dbshac2hire.thankcoin.ledger.api.controller;

public class CreateAccountRequest {
	private String number;
	private boolean open;
	
	public String getNumber() {
		return number;
	}
	public boolean isOpen() {
		return open;
	}
}