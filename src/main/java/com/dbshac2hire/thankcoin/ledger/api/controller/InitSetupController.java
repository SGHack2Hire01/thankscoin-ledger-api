package com.dbshac2hire.thankcoin.ledger.api.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbshac2hire.thankcoin.ledger.api.domain.Account;
import com.dbshac2hire.thankcoin.ledger.api.domain.Transaction;
import com.dbshac2hire.thankcoin.ledger.api.domain.Transaction.Type;
import com.dbshac2hire.thankcoin.ledger.api.repository.AccountRepository;
import com.dbshac2hire.thankcoin.ledger.api.repository.TransactionRepository;

@Controller
@EnableAutoConfiguration
public class InitSetupController {
	private static final Logger log = Logger.getLogger(InitSetupController.class);

	private static final int CORPORATE_ACCOUNT_BALANCE = 1000000000;
	private static final String CORPORATE_ACCOUNT_NAME = "corporate";

	@Autowired
	AccountRepository accountRepository;
	@Autowired
	TransactionRepository transcationRepository;

	@RequestMapping("/init")
	@ResponseBody
	String init() {
		Date currentTime = new Date();
		try {
			accountRepository.save(
					new Account(CORPORATE_ACCOUNT_NAME, false, CORPORATE_ACCOUNT_BALANCE, currentTime, currentTime));
			transcationRepository.save(new Transaction(Type.CREATE, null, CORPORATE_ACCOUNT_NAME,
					CORPORATE_ACCOUNT_BALANCE, currentTime, "initial top up", null));

			return "Init is done";
		} catch (Exception e) {
			log.error("Cannot init properly", e);

			return "Cannot init properly";
		}
	}
}
